def HanoisTårn(A, B, C, n):
    if (n == 1):
        print("Brikke " + str(n) + " fra " + str(A) + " til " + str(C))
    elif n > 1:
        HanoisTårn(A,C,B,n-1)
        print("Brikke " + str(n) + " fra " + str(A) + " til " + str(C));
        HanoisTårn(B,A,C,n-1)



HanoisTårn('A','B','C',7);