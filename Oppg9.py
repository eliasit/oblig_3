
# Boblesort algoritmen
def bubbleSort(a):
    for itr in range(len(a)-1, 0, -1):
        for i in range(itr):
            if a[i] > a[i+1]:
                temp = a[i]
                a[i] = a[i+1]
                a[i+1] = temp


a = [9,8,7,7,6,5,4,3,2,1]

bubbleSort(a)

print(a)
